CC=g++
CFLAGS=-I.
prefix=/usr
progname=simple-calc
all_objects=main.o calc_syntax.o expression.o strsep_addons.o

all: $(all_objects)
	$(CC) -o $(progname) $(all_objects)
	
expression.o: expression.cpp expression.h
	$(CC) -c expression.cpp $(CFLAGS)

calc_syntax.o: calc_syntax.cpp calc_syntax.h expression.h strsep_addons.h
	$(CC) -c calc_syntax.cpp $(CFLAGS)
	
main.o: main.cpp calc_syntax.h expression.h strsep_addons.h
	$(CC) -c main.cpp $(CFLAGS)
	
strsep_addons.o: strsep_addons.cpp strsep_addons.h
	$(CC) -c strsep_addons.cpp $(CFLAGS)

debug: main.cpp calc_syntax.cpp expression.cpp strsep_addons.cpp calc_syntax.h expression.h strsep_addons.h
	$(CC) -g -ggdb -o $(progname) main.cpp calc_syntax.cpp expression.cpp strsep_addons.cpp $(CFLAGS)

install: $(progname)
	install -m 0755 $(progname) $(prefix)/bin

uninstall:
	-rm $(prefix)/bin/$(progname)

windoze: main.cpp calc_syntax.cpp expression.cpp strsep_addons.cpp calc_syntax.h expression.h strsep_addons.h
	i586-mingw32msvc-g++ -g -ggdb -o $(progname).exe main.cpp calc_syntax.cpp expression.cpp strsep_addons.cpp $(CFLAGS)

clean: clean_o
	-rm -f $(progname) $(progname).exe

clean_o:
	-rm -f $(all_objects)
