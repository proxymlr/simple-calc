/*
 * expression.h
 * Copyright (C) 2014-2015  Mikhail Bazhanov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CALC_EXPRESSION_H
#define CALC_EXPRESSION_H

#include <list>
#include <map>
#include <string>
//#include <string.h>
////#include <stdio.h>

using std::list;
using std::map;
using std::string;

enum EXP_TYPE {
	EXP_UNKNOWN,
	EXP_NUMBER,
	EXP_FUNCTION,
	EXP_OPERATION,
	EXP_BOOLEAN_OPERATION,
	EXP_VARIABLE,
	EXP_CASE
};

class CalcExpression {
public:
	virtual CalcExpression* eval() const;
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
	virtual EXP_TYPE getType() const;
	virtual ~CalcExpression();
};

class NumberExpression: public CalcExpression {
protected:
	double value;
public:
	NumberExpression (double v);
	double getValue() const;
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
	virtual EXP_TYPE getType() const;
	virtual ~NumberExpression();
};

class OperationExpression: public CalcExpression {
protected:
	CalcExpression *left;
	CalcExpression *right;
public:
	OperationExpression (CalcExpression *l, CalcExpression *r);
	virtual CalcExpression* getLeft() const;
	virtual CalcExpression* getRight() const;
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars, double (*oper)(double,double)) const;
	virtual EXP_TYPE getType() const;
	virtual ~OperationExpression();
};

class SumExpression: public OperationExpression { // "+"
public:
	SumExpression (CalcExpression *l, CalcExpression *r);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
};

class MinusExpression: public OperationExpression { // "-"
public:
	MinusExpression (CalcExpression *l, CalcExpression *r);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
};

class DivExpression: public OperationExpression { // "/"
public:
	DivExpression (CalcExpression *l, CalcExpression *r);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
};

class MultExpression: public OperationExpression { // "*"
public:
	MultExpression (CalcExpression *l, CalcExpression *r);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
};

class VarExpression: public CalcExpression { // local variable (function parameter)
protected:
	string name;
public:
	VarExpression (const string &n);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
	virtual EXP_TYPE getType() const;
	const string& getName() const;
	virtual ~VarExpression();
};

class FuncExpression: public CalcExpression { // только справа от знака равно (вызов пользовательской либо встроенной функции)
protected:
	string name; // имя вызываемой функции
	list<CalcExpression*> args; // параметры вызова
public:
	FuncExpression (const string &n, const list<CalcExpression*> &a);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars_) const; // вычислит значение функции
	virtual EXP_TYPE getType() const;
	const string& getName() const;
	virtual ~FuncExpression();
};

class UserFuncExpression: public FuncExpression { // только слева от знака равно (определение пользовательской функции)
protected:
	//string name; // имя задаваемой функции
	//list<VarExpression*> args; // аргументы задаваемой функции
	CalcExpression *value;
public:
	UserFuncExpression (const string &n, const list<CalcExpression*> &a, CalcExpression *v);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const; // всегда вернет NULL
	virtual CalcExpression* getValue() const;
	virtual ~UserFuncExpression();
};


class BooleanOperationExpression: public OperationExpression {
public:
	BooleanOperationExpression (CalcExpression *l, CalcExpression *r);
	virtual EXP_TYPE getType() const;
};

class AndExpression: public BooleanOperationExpression { // &&
public:
	AndExpression (CalcExpression *l, CalcExpression *r);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
};

class OrExpression: public BooleanOperationExpression { // ||
public:
	OrExpression (CalcExpression *l, CalcExpression *r);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
};

class LessExpression: public BooleanOperationExpression { // <
public:
	LessExpression (CalcExpression *l, CalcExpression *r);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
};

class GreaterExpression: public BooleanOperationExpression { // >
public:
	GreaterExpression (CalcExpression *l, CalcExpression *r);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
};

class EqualsExpression: public BooleanOperationExpression { // ==
public:
	EqualsExpression (CalcExpression *l, CalcExpression *r);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
};

class LessOrEqualsExpression: public BooleanOperationExpression { // <=
public:
	LessOrEqualsExpression (CalcExpression *l, CalcExpression *r);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
};

class GreaterOrEqualsExpression: public BooleanOperationExpression { // >=
public:
	GreaterOrEqualsExpression (CalcExpression *l, CalcExpression *r);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
};

class NotEqualsExpression: public BooleanOperationExpression { // !=
public:
	NotEqualsExpression (CalcExpression *l, CalcExpression *r);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
};

class CaseExpression: public CalcExpression {
protected:
	map <CalcExpression*, CalcExpression*> IfThen;
	CalcExpression *Else;
public:
	CaseExpression (map<CalcExpression*,CalcExpression*> &IfThen_, CalcExpression *Else_);
	virtual CalcExpression* eval (const map<string,CalcExpression*> &TempVars) const;
	virtual EXP_TYPE getType() const;
	virtual ~CaseExpression();
};

void fullClearListOfCalcExpression (list<CalcExpression*> &L);
void fullClearMapOfTwoCalcExpression (map<CalcExpression*,CalcExpression*> &M);
void eraseUserFuncExpressions();
bool free_func_name (const string &name);
bool addFuncToUserFuncExpressions (const string &name, UserFuncExpression *userFunc);

//extern list<UserFuncExpression*> UserFuncExpressions;
////extern map<string,CalcExpression*> TempVars;

#endif
