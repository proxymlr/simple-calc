/*
 * strsep_addons.cpp
 * Copyright (C) 2014-2015  Mikhail Bazhanov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "strsep_addons.h"
#include <string.h>

bool is_char_in_sep (const char h, const char *sep) {
	bool was = false;
	int pos = 0;
	while (sep[pos] != 0) {
		if (h == sep[pos]) {
			return true;
		}
		++pos;
	}
	return false;
}

list<const char*> my_strsep (char *str, const char *sep, bool onlyOne) { // warning: it modifies input 'str'; 'sep' is array of char separators
	list<const char*> res; // warning: you must not manually remove res[i], because it is part of input 'str'
	if (onlyOne) {
		int len = strlen(str);
		bool need = is_char_in_sep('=',sep);
		for (int i=0; i<len; ++i) {
			if (is_char_in_sep(str[i],sep)) {
				if ((i>0 && ((need && (str[i-1]=='<' || str[i-1]=='>' || str[i-1]=='!')) || is_char_in_sep(str[i-1],sep))) || (i<len-1 && is_char_in_sep(str[i+1],sep))) {
					continue;
				}
				if (i == 0) {
					str[i] = 0;
					str += 1;
				} else if (i == len-1) {
					str[i] = 0;
				} else {
					str[i] = 0;
					res.push_back (str);
					str += i+1;
				}
			}
		}
		if (str[0] != 0) {
			res.push_back (str);
		}
		return res;
	} else {
		while (true) {
			while (str[0]!=0 && is_char_in_sep(str[0],sep)) {
				str += 1;
			}
			if (str[0]==0) {
				break;
			}
			//bool was = false;
			int pos = 1;
			while (str[pos]!=0 && !is_char_in_sep(str[pos],sep)) {
				//was = true;
				++pos;
			}
			//if (!was) {
			//	break;
			//}
			bool was = true;
			if (str[pos] == 0) {
				was = false; // не было сепаратора в конце
			} else {
				str[pos] = 0;
			}
			res.push_back (str);
			if (!was) {
				break;
			}
			str += pos+1;
		}
		return res;
	}
}

/*list<const char*> my_strsep(char *str, const char *sep) { 
	list<const char*> res; 

	bool foundString = false;
	int i = 0;
	char *ukazatel = str;
	while (str[i] != 0) {
		if (is_char_in_sep(str[i], sep)) {
			if (foundString) {
				res.push_back(ukazatel);
				str[i] = 0;
				foundString = false;
			}
			ukazatel = str + i + 1;

		} else {
			foundString = true;
		}	
		i++;
	}

	if (foundString) {
		res.push_back(ukazatel);	
	}

	return res;
}*/

char* my_strdup_without_spaces (const char *str, int len_) {
	int len = len_;
	if (len < 0) {
		len = strlen(str);
	}
	char *res = new char[len+1];
	int pos_old = 0, pos_new = 0;
	while (pos_old < len) {
		if (!is_char_in_sep (str[pos_old], " \t\n\r\v\f\a\b")) {
			res[pos_new] = str[pos_old];
			++pos_new;
		}
		++pos_old;
	}
	res[pos_new] = 0;
	return res;
}
