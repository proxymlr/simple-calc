/*
 * expression.cpp
 * Copyright (C) 2014-2015  Mikhail Bazhanov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "expression.h"
#include <math.h>
//#include <stdio.h>

static list<UserFuncExpression*> UserFuncExpressions;

CalcExpression* CalcExpression::eval() const {
	map<string,CalcExpression*> TempVars; 
	return this->eval (TempVars); // empty TempVars
}

CalcExpression* CalcExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	return NULL; // must be redefined in subclasses
}

CalcExpression* OperationExpression::eval (const map<string,CalcExpression*> &TempVars, double (*oper)(double,double)) const {
	CalcExpression *l = getLeft()->eval (TempVars);
	if (!l) {
		return NULL;
	}
	CalcExpression *r = getRight()->eval (TempVars);
	if (!r) {
		delete l;
		return NULL;
	}
	if (l->getType()==EXP_NUMBER && r->getType()==EXP_NUMBER) {
		NumberExpression *ln = (NumberExpression*)l;
		NumberExpression *rn = (NumberExpression*)r;
		NumberExpression *res = new NumberExpression (oper (ln->getValue(), rn->getValue()));
		delete ln;
		delete rn;
		return res;
	} else {
		return NULL; // maybe we need to return UnknownValueExpression ?
	}
}

EXP_TYPE CalcExpression::getType() const {
	return EXP_UNKNOWN;
}

EXP_TYPE NumberExpression::getType() const {
	return EXP_NUMBER;
}

EXP_TYPE OperationExpression::getType() const {
	return EXP_OPERATION;
}

EXP_TYPE BooleanOperationExpression::getType() const {
	return EXP_BOOLEAN_OPERATION;
}

EXP_TYPE VarExpression::getType() const {
	return EXP_VARIABLE;
}

EXP_TYPE FuncExpression::getType() const {
	return EXP_FUNCTION;
}

EXP_TYPE CaseExpression::getType() const {
	return EXP_CASE;
}

CalcExpression* NumberExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	//return this;
	return new NumberExpression (value);
}

NumberExpression::NumberExpression (double v): value(v) {
}

double NumberExpression::getValue() const {
	return value;
}

OperationExpression::OperationExpression (CalcExpression *l, CalcExpression *r): left(l), right(r) {
}

CalcExpression* OperationExpression::getLeft() const {
	return left;
}

CalcExpression* OperationExpression::getRight() const {
	return right;
}

CalcExpression* OperationExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	return NULL; // must be redefined in subclasses
}


SumExpression::SumExpression (CalcExpression *l, CalcExpression *r): OperationExpression(l,r) {
}

static double mySum (double l, double r) {
	return l + r;
}

CalcExpression* SumExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	CalcExpression *res = ((OperationExpression*)this)->eval (TempVars, &mySum);
	return res;
}

MinusExpression::MinusExpression (CalcExpression *l, CalcExpression *r): OperationExpression(l,r) {
}

static double myMinus (double l, double r) {
	return l - r;
}

CalcExpression* MinusExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	CalcExpression *res = ((OperationExpression*)this)->eval (TempVars, &myMinus);
	return res;
}

DivExpression::DivExpression (CalcExpression *l, CalcExpression *r): OperationExpression(l,r) {
}

static double myDiv (double l, double r) {
	return l / r;
}

CalcExpression* DivExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	CalcExpression *res = ((OperationExpression*)this)->eval (TempVars, &myDiv);
	return res;
}

MultExpression::MultExpression (CalcExpression *l, CalcExpression *r): OperationExpression(l,r) {
}

static double myMulti (double l, double r) {
	return l * r;
}

CalcExpression* MultExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	CalcExpression *res = ((OperationExpression*)this)->eval (TempVars, &myMulti);
	return res;
}

VarExpression::VarExpression (const string &n): name(n) {
}

CalcExpression* VarExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	map<string,CalcExpression*>::const_iterator i = TempVars.find (name);
	if (i == TempVars.end()) {
		return new VarExpression (name);
	} else {
		return (*i).second->eval();
	}
}

CalcExpression::~CalcExpression() {
}

NumberExpression::~NumberExpression() {
}

OperationExpression::~OperationExpression() {
	if (left) {
		delete left;
		left = NULL;
	}
	if (right) {
		delete right;
		right = NULL;
	}
}

VarExpression::~VarExpression() {
}

const string& VarExpression::getName() const {
	return name;
}

FuncExpression::FuncExpression (const string &n, const list<CalcExpression*> &a): name(n), args(a) {
}

const string& FuncExpression::getName() const {
	return name;
}

FuncExpression::~FuncExpression() {
	for (list<CalcExpression*>::iterator i=args.begin(); i!=args.end(); ++i) {
		delete (*i);
	}
	args.clear();
}

UserFuncExpression::UserFuncExpression (const string &n, const list<CalcExpression*> &a, CalcExpression *v): FuncExpression(n,a), value(v) {
}

UserFuncExpression::~UserFuncExpression() {
	delete value;
}

CalcExpression* UserFuncExpression::eval (const map<string,CalcExpression*> &TempVars) const { // if you call this funciton for this class, then it is logical error
	return NULL;
}

CalcExpression* UserFuncExpression::getValue() const {
	return value;
}

CalcExpression* FuncExpression::eval (const map<string,CalcExpression*> &TempVars_) const {
	CalcExpression *res = NULL;
	int size = args.size();
	//printf("size: %i\n",size);
	if (size == 0) {
		if (name.compare("pi") == 0) {
			return new NumberExpression (3.14);
		} else if (name.compare("e") == 0) {
			return new NumberExpression (2.718);
		}
	} else if (size >= 1) {
		CalcExpression *arg0 = (*(args.begin()))->eval (TempVars_);
		NumberExpression *arg0_num = NULL;
		double arg0_value;
		if (arg0 && arg0->getType()==EXP_NUMBER) {
			arg0_num = (NumberExpression*)arg0;
			arg0_value = arg0_num->getValue();
		}
		if (arg0_num && size==1) {
			if (name.compare("sin") == 0) {
				res = new NumberExpression (sin(arg0_value));
			} else if (name.compare("cos") == 0) {
				res = new NumberExpression (cos(arg0_value));
			} else if (name.compare("tan") == 0) {
				res = new NumberExpression (tan(arg0_value));
			} else if (name.compare("atan") == 0) {
				res = new NumberExpression (atan(arg0_value));
			} else if (name.compare("asin") == 0) {
				res = new NumberExpression (asin(arg0_value));
			} else if (name.compare("acos") == 0) {
				res = new NumberExpression (acos(arg0_value));
			} else if (name.compare("exp") == 0) {
				res = new NumberExpression (exp(arg0_value));
			} else if (name.compare("ln") == 0) {
				res = new NumberExpression (log(arg0_value));
			} else if (name.compare("log10") == 0) {
				res = new NumberExpression (log10(arg0_value));
			}
		} else if (arg0_num && size==2) {
			CalcExpression *arg1 = (*(args.rbegin()))->eval (TempVars_);
			NumberExpression *arg1_num = NULL;
			if (arg1 && arg1->getType()==EXP_NUMBER) {
				arg1_num = (NumberExpression*)arg1;
			}
			if (arg1_num) {
				double arg1_value = arg1_num->getValue();
				if (name.compare("pow") == 0) {
					res = new NumberExpression (pow(arg0_value,arg1_value));
				} else if (name.compare("log") == 0) {
					res = new NumberExpression (log(arg1_value)/log(arg0_value));
				}
			}
			arg1_num = NULL;
			if (arg1) {
				delete arg1;
				arg1 = NULL;
			}
		}
		arg0_num = NULL;
		if (arg0) {
			delete arg0;
			arg0 = NULL;
		}
		if (res) {
			return res;
		}
	}
	
	// for user functions
	for (list<UserFuncExpression*>::iterator i=UserFuncExpressions.begin(); i!=UserFuncExpressions.end(); ++i) {
		UserFuncExpression *user_func = *i;
		//printf("some user func: %s \n",user_func->getName().c_str());
		if (name.compare(user_func->getName()) == 0) {
			//printf("{found!}\n");
			if (user_func->args.size() != size) {
				//printf("user error: incorrect argumants count\n");
				return NULL; // user error: incorrect argumants count
			}
			list<CalcExpression*> values;
			bool bad = false;
			for (list<CalcExpression*>::const_iterator j=args.begin(); j!=args.end(); ++j) {
				CalcExpression* value = (*j)->eval (TempVars_);
				if (value) {
					values.push_back (value);
				}
				if (!value || value->getType()!=EXP_NUMBER) {
					bad = true;
					break;
				}
			}
			//printf("{post}\n");
			if (!bad) {
				map<string,CalcExpression*> TempVars; // TempVars is now local, not global
				for (list<CalcExpression*>::iterator k=user_func->args.begin(),v=values.begin(); v!=values.end(); ++k,++v) {
					CalcExpression *key = (*k);
					if (key->getType()!=EXP_VARIABLE) {
						TempVars.clear();
						return NULL; // logical error of program
					}
					TempVars[((VarExpression*)key)->getName()] = (*v);
				}
                //printf("{eval}\n");
				res = user_func->getValue()->eval(TempVars);
                //printf("{clear}\n");
				TempVars.clear();
			}
			for (list<CalcExpression*>::iterator v=values.begin(); v!=values.end(); ++v) {
				delete (*v);
			}
			values.clear();
			return res;
		}
	}
	//printf("user error: unknown function\n");
	return NULL; // user error: unknown function
}


BooleanOperationExpression::BooleanOperationExpression (CalcExpression *l, CalcExpression *r): OperationExpression(l,r) {
}

AndExpression::AndExpression (CalcExpression *l, CalcExpression *r): BooleanOperationExpression(l,r) {
}

static double myAnd (double l, double r) {
	return l && r;
}

CalcExpression* AndExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	CalcExpression *res = ((OperationExpression*)this)->eval (TempVars, &myAnd);
	return res;
}

OrExpression::OrExpression (CalcExpression *l, CalcExpression *r): BooleanOperationExpression(l,r) {
}

static double myOr (double l, double r) {
	return l || r;
}

CalcExpression* OrExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	CalcExpression *res = ((OperationExpression*)this)->eval (TempVars, &myOr);
	return res;
}

LessExpression::LessExpression (CalcExpression *l, CalcExpression *r): BooleanOperationExpression(l,r) {
}

static double myLess (double l, double r) {
	return l < r;
}

CalcExpression* LessExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	CalcExpression *res = ((OperationExpression*)this)->eval (TempVars, &myLess);
	return res;
}

GreaterExpression::GreaterExpression (CalcExpression *l, CalcExpression *r): BooleanOperationExpression(l,r) {
}

static double myGreater (double l, double r) {
	return l > r;
}

CalcExpression* GreaterExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	CalcExpression *res = ((OperationExpression*)this)->eval (TempVars, &myGreater);
	return res;
}

EqualsExpression::EqualsExpression (CalcExpression *l, CalcExpression *r): BooleanOperationExpression(l,r) {
}

static double myEquals (double l, double r) {
	return l == r;
}

CalcExpression* EqualsExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	CalcExpression *res = ((OperationExpression*)this)->eval (TempVars, &myEquals);
	return res;
}

LessOrEqualsExpression::LessOrEqualsExpression (CalcExpression *l, CalcExpression *r): BooleanOperationExpression(l,r) {
}

static double myLessOrEquals (double l, double r) {
	return l <= r;
}

CalcExpression* LessOrEqualsExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	CalcExpression *res = ((OperationExpression*)this)->eval (TempVars, &myLessOrEquals);
	return res;
}

GreaterOrEqualsExpression::GreaterOrEqualsExpression (CalcExpression *l, CalcExpression *r): BooleanOperationExpression(l,r) {
}

static double myGreaterOrEquals (double l, double r) {
	return l >= r;
}

CalcExpression* GreaterOrEqualsExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	CalcExpression *res = ((OperationExpression*)this)->eval (TempVars, &myGreaterOrEquals);
	return res;
}

NotEqualsExpression::NotEqualsExpression (CalcExpression *l, CalcExpression *r): BooleanOperationExpression(l,r) {
}

static double myNotEquals (double l, double r) {
	return l != r;
}

CalcExpression* NotEqualsExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	CalcExpression *res = ((OperationExpression*)this)->eval (TempVars, &myNotEquals);
	return res;
}

CaseExpression::CaseExpression (map<CalcExpression*,CalcExpression*> &IfThen_, CalcExpression *Else_): IfThen(IfThen_), Else(Else_) {
	IfThen_.clear();
	Else_ = NULL;
}

CaseExpression::~CaseExpression() {
	if (Else) {
		delete Else;
		Else = NULL;
	}
	fullClearMapOfTwoCalcExpression (IfThen);
}

CalcExpression* CaseExpression::eval (const map<string,CalcExpression*> &TempVars) const {
	bool was = false;
	CalcExpression *res = NULL;
	for (map<CalcExpression*,CalcExpression*>::const_iterator i=IfThen.begin(); i!=IfThen.end(); ++i) {
		CalcExpression *If   = i->first->eval (TempVars);
		if (If) {
			if (If->getType()==EXP_NUMBER && ((NumberExpression*)If)->getValue() != 0) { // if TRUE
				CalcExpression *Then = i->second->eval (TempVars);
				res = Then;
				was = true;
			}
			delete If;  If = NULL;
			if (was) {
				break;
			}
		}
	}
	if (!was && Else) {
		res = Else->eval (TempVars);
	}
	return res;
}

void fullClearListOfCalcExpression (list<CalcExpression*> &L) {
	for (list<CalcExpression*>::iterator i=L.begin(); i!=L.end(); ++i) {
		if (*i) {
			delete *i;
		}
	}
	L.clear();
}

void fullClearMapOfTwoCalcExpression (map<CalcExpression*,CalcExpression*> &M) {
	for (map<CalcExpression*,CalcExpression*>::iterator i=M.begin(); i!=M.end(); ++i) {
		CalcExpression *key   = i->first;
		CalcExpression *value = i->second;
		delete key;
		delete value;
	}
	M.clear();
}

void eraseUserFuncExpressions() {
	fullClearListOfCalcExpression (*((list<CalcExpression*>*)&UserFuncExpressions));
	/*for (list<UserFuncExpression*>::iterator i=UserFuncExpressions.begin(); i!=UserFuncExpressions.end(); ++i) {
		delete (*i);
	}
	UserFuncExpressions.clear();*/
}

bool free_func_name (const string &name) {
	for (list<UserFuncExpression*>::iterator i=UserFuncExpressions.begin(); i!=UserFuncExpressions.end(); ++i) {
		if ((*i)->getName() == name) {
			delete (*i);
			UserFuncExpressions.erase(i);
			return true;
		}
	}
	return false;
}

bool addFuncToUserFuncExpressions (const string &name, UserFuncExpression *userFunc) {
	if (!userFunc) {
		return false;
	}
	free_func_name (name);
	UserFuncExpressions.push_back (userFunc);
	return true;
}
