/*
 * strsep_addons.h
 * Copyright (C) 2014-2015  Mikhail Bazhanov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef STRSEP_ADDONS_H
#define STRSEP_ADDONS_H

#include <list>
#include <map>

using std::list;
using std::map;

bool is_char_in_sep (const char h, const char *sep);
list<const char*> my_strsep (char *str, const char *sep, bool onlyOne=false);
char* my_strdup_without_spaces (const char *str, int len_=-1);

#endif
