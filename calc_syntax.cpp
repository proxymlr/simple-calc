/*
 * calc_syntax.cpp
 * Copyright (C) 2014-2015  Mikhail Bazhanov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "calc_syntax.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strsep_addons.h"

static CalcExpression* syntax_parse_right (const char *str_right, int len_right);

static bool syntax_parse_right__number (const char *str_right, int len_right, int &pos_new, bool &wasLenMax, bool &was) {
	while (str_right[pos_new]=='.' || (str_right[pos_new]>='0' && str_right[pos_new]<='9')) {
		was = true;
		if ((++pos_new)>=len_right) {
			wasLenMax = true;
			break;
		}
	}
	if (was && !wasLenMax && (str_right[pos_new]=='e' || str_right[pos_new]=='E')) {
		if ((++pos_new) >= len_right) { // bad
			return false;
		}
		if (str_right[pos_new]=='-' || str_right[pos_new]=='+' || (str_right[pos_new]>='0' && str_right[pos_new]<='9')) {
			do {
				if ((++pos_new)>=len_right) {
					wasLenMax = true;
					break;
				}
			} while (str_right[pos_new]>='0' && str_right[pos_new]<='9');
		} else { // bad
			return false;
		}
	}
	return true;
}

static bool syntax_parse_right__brackets (const char *str_right, int len_right, int &pos_new, bool &wasLenMax, bool &was, const char leftBracket, const char rightBracket) {
	was = true;
	int brackets = 1;
	do {
		if ((++pos_new)>=len_right) {
			wasLenMax = true;
			break;
		}
		if (str_right[pos_new]==rightBracket) {
			--brackets;
		} else if (str_right[pos_new]==leftBracket) {
			++brackets;
		}
	} while (brackets>0);
	if (brackets != 0) { // bad
		return false;
	} else if (!wasLenMax && str_right[pos_new]==rightBracket) {
		if ((++pos_new)>=len_right) {
			wasLenMax = true;
		}
	}		
	return true;
}

static bool syntax_parse__letters (const char *str_right, int len_right, int &pos_new, bool &wasLenMax, bool &was) {
	while (pos_new<len_right && ((str_right[pos_new]>='a' && str_right[pos_new]<='z') || (str_right[pos_new]>='A' && str_right[pos_new]<='Z'))) {
		was = true;
		++pos_new;
	}
	return true;
}

static CalcExpression* syntax_parse__func (const char *str_right, int len_right, int &pos, int &pos_new, bool &wasLenMax, bool &was, bool isRightFunc, CalcExpression *resRight) {
	CalcExpression *lastRight = NULL;
	if (!syntax_parse__letters (str_right, len_right, pos_new, wasLenMax, was)) {
		return NULL;
	}
	if (was) {
		string name = string (str_right+pos, pos_new-pos);
		if (pos_new<len_right && str_right[pos_new]=='(') {
			++pos_new;
			if (pos_new<len_right) {				
				int brackets = 1;
				bool was_sub = false;
				list<CalcExpression*> subs;
				bool bad_subs = false;
				pos = pos_new;
				if (str_right[pos] == ')') {
					brackets = 0;
					++pos_new;
				} 
				while (brackets>0 && pos_new<len_right) {
					if (str_right[pos_new]=='(') {
						++brackets;
					} else if (str_right[pos_new]==')') {
						--brackets;
						if (brackets == 0) {
							was_sub = true;
						}
					} else if (str_right[pos_new]==',') {
						if (brackets == 1) {
							was_sub = true;
						}
					}
					if (was_sub) {
						if (!(brackets==0 && pos_new==pos && str_right[pos]==')' && str_right[pos-1]=='(')) {
							CalcExpression *var_expr = syntax_parse_right (str_right+pos, pos_new-pos);
							if (!var_expr) { // bad
								bad_subs = true;
								break;
							}
							if (!isRightFunc && var_expr->getType()!=EXP_VARIABLE) { // bad
								delete var_expr;
								bad_subs = true;
								break;
							}
							subs.push_back (var_expr);
						}
						was_sub = false;
						++pos_new;
						pos = pos_new; 
					} else {
						++pos_new;
					}
				}
				if (brackets==0 && !bad_subs) {
					if (isRightFunc) {
						lastRight = new FuncExpression (name, subs);
					} else {
						UserFuncExpression *real_res = new UserFuncExpression (name, subs, resRight);
						subs.clear();
						//free_func_name (name);
						//UserFuncExpressions.push_back (real_res);
						addFuncToUserFuncExpressions (name, real_res);
						//printf("user function %s is added\n",name.c_str());
						lastRight = real_res;
						return lastRight;
					}
					//subs.clear();
				}
				if (lastRight == NULL) {
					for (list<CalcExpression*>::const_iterator i=subs.begin(); i!=subs.end(); ++i) {
						CalcExpression *e = *i;
						if (e != NULL) {
							delete e;
						}
					}
				}
				subs.clear();
				
				if (lastRight == NULL) {
					return NULL;
				}
			} else { // bad
				return NULL;
			}
		} else {
			if (isRightFunc) {
				lastRight = new VarExpression (name);
			}
		}
	} else { // bad
		return NULL;
	}
	return lastRight;
}

static CalcExpression* syntax_parse_right__func (const char *str_right, int len_right, int &pos, int &pos_new, bool &wasLenMax, bool &was) {
	return syntax_parse__func (str_right, len_right, pos, pos_new, wasLenMax, was, true, NULL);
}

static CalcExpression* syntax_parse_left__func (const char *str_right, int len_right, int &pos, int &pos_new, bool &wasLenMax, bool &was, CalcExpression *resRight) {
	return syntax_parse__func (str_right, len_right, pos, pos_new, wasLenMax, was, false, resRight);
}

static bool syntax_parse__IfThenElse (const char *str_right, int len_right, map<CalcExpression*,CalcExpression*> &IfThen, CalcExpression *&Else) { // TODO
	char *running_str = my_strdup_without_spaces (str_right, len_right);
	const char delimiters[] = "|";
	int len = strlen (running_str);
	if (len == 0) {
		return false;
	}
	CalcExpression *If = NULL;
	CalcExpression *Then = NULL;
	bool bad = false;
	list<const char*> tokens = my_strsep (running_str, delimiters, true);
	if (!tokens.empty() && strlen(*(tokens.begin()))<len) {
		list<const char*>::const_iterator token_str = tokens.begin();
		const char *str_left = *token_str;
		++token_str;
		if (token_str != tokens.end()) {
			const char *str_right = *token_str;
			++token_str;
			if (token_str == tokens.end()) {
				Then = syntax_parse_right (str_left, strlen(str_left));
				if (!Then) {
					bad = true;
				} else {
					If = syntax_parse_right (str_right, strlen(str_right));
					if (!If) {
						Else = Then;
						Then = NULL;
						bad = false;
					} else {
						IfThen[If] = Then;
						If = NULL;
						Then = NULL;
						bad = false;
					}
				}
			} else {
				bad = true;
				//printf (" !error_1\n");
			}
		} else {
			bad = true;
			//printf (" !error_2\n");
		}
		tokens.clear();
	} else {
		bad = true;
	}
	delete[] running_str;
	
	if (bad) {
		Else = syntax_parse_right (str_right, len_right);
		if (Else) {
			bad = false;
		}
	}
	if (bad) {
		if (Else) {
			delete Else;  Else = NULL;
		}
		fullClearMapOfTwoCalcExpression (IfThen);
	}
	return !bad;
}

static CalcExpression* syntax_parse__case (const char *str_right, int len_right) { // TODO
	map<CalcExpression*,CalcExpression*> IfThen;
	CalcExpression *Else = NULL;
	int brackets1 = 0; // {}
	int brackets2 = 0; // ()
	int pos = 0;
	int pos_new = 0;
	bool bad = false;
	while (pos_new < len_right) {
		if (str_right[pos_new] == '{') {
			++brackets1;
		} else if (str_right[pos_new] == '(') {
			++brackets2;
		} else if (str_right[pos_new] == ')') {
			--brackets2;
		} else if (str_right[pos_new] == '}') {
			--brackets1;
		} else if (str_right[pos_new] == ',' && brackets1+brackets2==0) {
			if (pos != pos_new) {
				bad = !syntax_parse__IfThenElse (str_right+pos, pos_new-pos, IfThen, Else);
				if (bad || Else) {
					break;
				}
				++pos_new;
				pos = pos_new;
				--pos_new;
			} else { // bad	
			}
		}
		++pos_new;
	}
	if (!bad && !Else && pos!=pos_new) {
		bad = !syntax_parse__IfThenElse (str_right+pos, pos_new-pos, IfThen, Else);
	}
	if (bad) {
		return NULL;
	} else {
		return new CaseExpression (IfThen, Else);
	}
}


static CalcExpression* syntax_parse_right (const char *str_right, int len_right) {
	//CalcExpression *res = NULL;
	//bool was_plus_minus = false; // was plus or minus operation?
	CalcExpression *lastPlusMinusLeft = NULL;
	CalcExpression *lastDivMultiLeft  = NULL;
	bool lastPlusMinusOper = false; // false==+, true==-
	bool lastDivMultiOper  = false; // false==*, true==/
	short int lastOperType = 0; // 0 - no operation, 1 - multi/div, 2 - plus/minus 
	bool lastWasUnarMunus  = false;
	int pos = 0;
	while (pos < len_right) {
		int repeats = 1;
		CalcExpression *preLastRight = NULL;
		CalcExpression *lastRight;
		char wasOperation = 'Z';
		char nextOperation;
		while (repeats > 0) {
			lastRight = NULL;
			nextOperation = '\0';
			--repeats;
			if (str_right[pos] == '+') {
				++pos;
			} else if (str_right[pos] == '-') {
				lastWasUnarMunus = true;
				++pos;
			}
			if (pos >= len_right) { // bad
				break;
			}
			bool wasLenMax = false;
			bool was = false;
			int pos_new = pos;
			if (!syntax_parse_right__number (str_right, len_right, pos_new, wasLenMax, was)) {
				break;
			}
			if (was) {
				double value = atof (str_right+pos);
				if (lastWasUnarMunus) {
					value = -value;
					lastWasUnarMunus = false;
				}
				lastRight = new NumberExpression (value);
			} else if (pos_new<len_right && str_right[pos_new]=='(') {
				if (!syntax_parse_right__brackets (str_right, len_right, pos_new, wasLenMax, was,'(',')')) {
					break;
				}
				lastRight = syntax_parse_right (str_right+pos+1, pos_new-pos-2); // between brackets
				if (lastRight == NULL) { // bad
					break;
				}
			} else if (pos_new<len_right && str_right[pos_new]=='{') {
				if (!syntax_parse_right__brackets (str_right, len_right, pos_new, wasLenMax, was,'{','}')) {
					break;
				}
				lastRight = syntax_parse__case (str_right+pos+1, pos_new-pos-2); // between brackets
				if (lastRight == NULL) { // bad
					break;
				}
			} else {
				//was = false;
				lastRight = syntax_parse_right__func (str_right, len_right, pos, pos_new, wasLenMax, was);
				if (!lastRight) {
					break;
				}
			}
			if (lastWasUnarMunus) {
				lastRight = new MultExpression (new NumberExpression(-1), lastRight);
				lastWasUnarMunus = false;
			}
			pos = pos_new;
			
			if (pos<len_right) {
				nextOperation = str_right[pos++];
				if (nextOperation=='&' || nextOperation=='|' || nextOperation=='=') {
					if (pos<len_right) {
						nextOperation = str_right[pos++];
						if (nextOperation != str_right[pos-2]) {
							nextOperation = 'z'; // 'z' is unknown operation
						}
					} else {
						nextOperation = 'z';
					}
				} else if (nextOperation=='>' || nextOperation=='<' || nextOperation=='!') {
					if (pos>=len_right) {
						nextOperation = 'z';
					} else {
						char wasOperation = nextOperation;
						nextOperation = str_right[pos++];
						if (nextOperation != '=') {
							--pos;
							nextOperation = wasOperation;
						} else {
							if (wasOperation == '>') {
								nextOperation = 'G';
							} else if (wasOperation == '<') {
								nextOperation = 'L';
							} else if (wasOperation == '!') {
								nextOperation = 'N';
							}
						}
					}
				}
			}
			if (preLastRight && wasOperation!='Z') {
				switch (wasOperation) {
					case '&': lastRight = new AndExpression (preLastRight, lastRight); break;
					case '|': lastRight = new OrExpression (preLastRight, lastRight); break;
					case '=': lastRight = new EqualsExpression (preLastRight, lastRight); break;
					case '>': lastRight = new GreaterExpression (preLastRight, lastRight); break;
					case '<': lastRight = new LessExpression (preLastRight, lastRight); break;
					case 'G': lastRight = new GreaterOrEqualsExpression (preLastRight, lastRight); break;
					case 'L': lastRight = new LessOrEqualsExpression (preLastRight, lastRight); break;
					case 'N': lastRight = new NotEqualsExpression (preLastRight, lastRight); break;
					default: { // logical error
						delete preLastRight;  preLastRight = NULL;
						delete lastRight; lastRight = NULL;
					}
				}
				preLastRight = NULL;
				wasOperation = 'Z';
			}
			if (nextOperation=='&' || nextOperation=='|' || nextOperation=='=' || nextOperation=='>' || nextOperation=='<' || nextOperation=='G' || nextOperation=='L' || nextOperation=='N') {
				++repeats;
				wasOperation = nextOperation;
				preLastRight = lastRight;
			}
		}
		
		if (nextOperation == '*' || nextOperation == '/') {
			lastOperType = 1;
			if (lastDivMultiLeft == NULL) {
				lastDivMultiLeft = lastRight;
				lastRight = NULL;
			}
			if (lastRight != NULL) {
				if (!lastDivMultiOper) {
					lastDivMultiLeft = new MultExpression (lastDivMultiLeft, lastRight);
				} else {
					lastDivMultiLeft = new DivExpression  (lastDivMultiLeft, lastRight);
				}
				lastRight = NULL;
			}
			if (nextOperation == '*') {
				lastDivMultiOper = false;
			} else {
				lastDivMultiOper = true;
			}
		} else if (nextOperation == '+' || nextOperation == '-') {
			if (lastOperType != 1) {
				if (lastPlusMinusLeft == NULL) {
					lastPlusMinusLeft = lastRight;
					lastRight = NULL;
					lastOperType = 2;
				}
				if (lastRight != NULL) {
					if (!lastPlusMinusOper) {
						lastPlusMinusLeft = new SumExpression (lastPlusMinusLeft, lastRight);
					} else {
						lastPlusMinusLeft = new MinusExpression  (lastPlusMinusLeft, lastRight);
					}
					lastRight = NULL;
					lastOperType = 2;
				}
			} else {
				if (lastRight != NULL) {
					CalcExpression *lastPlusMinusLeft_  = NULL;
					if (!lastDivMultiOper) {
						lastPlusMinusLeft_ = new MultExpression (lastDivMultiLeft, lastRight);
					} else {
						lastPlusMinusLeft_ = new DivExpression  (lastDivMultiLeft, lastRight);
					}
					if (lastPlusMinusLeft != NULL) {
						if (!lastPlusMinusOper) {
							lastPlusMinusLeft = new SumExpression (lastPlusMinusLeft, lastPlusMinusLeft_);
						} else {
							lastPlusMinusLeft = new MinusExpression (lastPlusMinusLeft, lastPlusMinusLeft_);
						}	
					} else {
						lastPlusMinusLeft = lastPlusMinusLeft_;
					}
					lastPlusMinusLeft_ = NULL;
					lastDivMultiLeft = NULL;
					lastRight = NULL;
					lastOperType = 2;
				}
			}
			if (nextOperation == '+') {
				lastPlusMinusOper = false;
			} else {
				lastPlusMinusOper = true;
			}
		} else if (nextOperation == '\0') {
			if (lastOperType == 2) {
				if (!lastPlusMinusOper) {
					return new SumExpression (lastPlusMinusLeft, lastRight);
				} else {
					return new MinusExpression (lastPlusMinusLeft, lastRight);
				}
			} else if (lastOperType == 0) {
				return lastRight;
			} else if (lastOperType == 1) {
				if (!lastDivMultiOper) {
					lastRight = new MultExpression (lastDivMultiLeft, lastRight);
				} else {
					lastRight = new DivExpression  (lastDivMultiLeft, lastRight);
				}
				lastDivMultiLeft = NULL;
				if (lastPlusMinusLeft != NULL) {
					if (!lastPlusMinusOper) {
						lastRight = new SumExpression (lastPlusMinusLeft, lastRight);
					} else {
						lastRight = new MinusExpression (lastPlusMinusLeft, lastRight);
					}
					lastPlusMinusLeft = NULL;
				}
				return lastRight;
			}
			break; // bad (logic)
		} else { // unknown operation
			if (lastRight) {
				delete lastRight;  lastRight = NULL;
			}
			break;
		}
	}
	if (lastPlusMinusLeft != NULL) {
		delete lastPlusMinusLeft;
		lastPlusMinusLeft = NULL;
	}
	if (lastDivMultiLeft != NULL) {
		delete lastDivMultiLeft;
		lastDivMultiLeft = NULL;
	}
	return NULL; // bad
}

CalcExpression* syntax_eval (const char *input_str, int len) {
	CalcExpression *r = syntax_parse_right (input_str, len);
	if (!r) {
		return NULL;
	}
	CalcExpression *res = r->eval();
	delete r;
	return res;
}

CalcExpression* syntax_assign (const char *str_left, int len_left, const char *str_right, int len_right) {
	CalcExpression *resRight = syntax_parse_right (str_right, len_right);
	if (resRight == NULL) { // bad
		//printf("1\n");
		return NULL;
	}
	
	int pos = 0;
	int pos_new = pos;
	bool was = false;
	bool wasLenMax = false;
	
	CalcExpression *realRes = syntax_parse_left__func (str_left, len_left, pos, pos_new, wasLenMax, was, resRight);
	if (realRes == NULL) {
		delete resRight;
		return NULL;
	}
	
	pos = pos_new;
	
	bool isF = realRes->getType()==EXP_FUNCTION;
	if (pos!=len_left || !isF) {
		if (isF) {
			string name = ((FuncExpression*)realRes)->getName();
			free_func_name (name);
		} else {
			delete realRes;
		}
		realRes = NULL;
	}
		
	return realRes;
}
