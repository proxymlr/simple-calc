/*
 * main.cpp
 * Copyright (C) 2014-2015  Mikhail Bazhanov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "calc_syntax.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strsep_addons.h"

//list<UserFuncExpression*> UserFuncExpressions;
////map<string,CalcExpression*> TempVars;

static void calculate (const char *str_left, const char *str_right) {
	////printf ("%s=%s", str_left, str_right);
	int len_left = strlen(str_left);
	int len_right = strlen(str_right);
	if (len_left==0 || len_right==0) {
		return; // bad
	}
	
	CalcExpression *res = syntax_assign (str_left, len_left, str_right, len_right);
	if (res!=NULL && res->getType()==EXP_FUNCTION) {
		printf (" .assigned\n");
	} else {
		printf (" !error_4\n");
	}
}

static void calculate (const char *input_str) {
	////printf ("calc:%s", input_str);
	int len = strlen (input_str);
	if (len == 0) {
		return;
	}
	const char delimiters_0[] = ";";
	char *running_str = my_strdup_without_spaces (input_str); // make writeble copy
	len = strlen (running_str);
	if (len == 0) {
		delete[] running_str;
		return;
	}
	char *token_str;
	bool was = false;
	
	list<const char*> tokens = my_strsep (running_str, delimiters_0);
	for (list<const char*>::const_iterator token_str=tokens.begin(); token_str!=tokens.end(); ++token_str) {
		if (strlen(*token_str) < len) {
			was = true;
		}
		if (was) {
			calculate (*token_str);
		}
	}
	tokens.clear();
	if (was) {
		delete[] running_str;
		return;
	}
	const char delimiters_1[] = "=";
	///running_str = strdupa (input_str); // make writeble copy
	tokens = my_strsep (running_str, delimiters_1, true);
	if (!tokens.empty() && strlen(*(tokens.begin()))<len) {
		list<const char*>::const_iterator token_str = tokens.begin();
		const char *str_left = *token_str;
		++token_str;
		if (token_str != tokens.end()) {
			const char *str_right = *token_str;
			++token_str;
			if (token_str == tokens.end()) {
				calculate (str_left, str_right);
			} else {
				printf (" !error_1\n");
			}
		} else {
			printf (" !error_2\n");
		}
		tokens.clear();
		delete[] running_str;
		return;
	}
	tokens.clear();
		
	CalcExpression *res = syntax_eval (running_str, len);
	if (res == NULL) {
		printf (" !error_3\n");
	} else if (res->getType()==EXP_NUMBER) {
		NumberExpression *number = (NumberExpression*)res;
		printf (" .res=%g\n", number->getValue());
	} else {
		printf (" ?unknown\n");
	}
	if (res) {
		delete res;
	}
	delete[] running_str;
}

static void print_help() {
	printf ("Type 'quit' to quit interactive mode.\n\
You can type expressions like '6+4*(2+3)+5*7'. This program can calculate it.\n\
You can type expressions line by line\n\
 or you can use ';' delimiter, e.g. '6+4*(2+3)+5*7;2+3'.\n\
You can define functions 'f(x,y)=6+y*(x+3)+5*7'\n\
 and then you can calculate it 'f(2,4)'\n\
 or create new funtion 'g(x)=f(4,x)+1'.\n\
You can use build-in funtions:\n\
 one argument: sin, cos, tan, atan, asin, acos, exp, ln, log10;\n\
 two arguments: log, pow.\n \
You can use predefined constants: 'pi()', 'e()'.\n\
You can define global variables as functions without paramaters: 'a()=5'.\n \
You can redefine funtions and global variables.\n\
You can calculate boolean expressions:\
 '((4>5)&&(4<=5))||(5==5)||(6!=6)||(7<5)||(9>=3)'\n \
You can define if-then-else cases: '{1|2==2,3}'\n \
You can define functions with cases:\n \
 'max(x,y)={x|x>=y,y}'\n\
 'fact(x)={1|x==0,fact(x-1)*x|x>0,-1}'\n\
 'f(x)={0|(x>=-1)&&(x<=1),x|x>1}'\n");
}

int main (int argc, char **argv) {
	if (argc == 1) {
		printf ("You also can run from command line: %s 'some_expression_to_calculate'.\n", argv[0]);
		printf ("Interactive mode activated. Type 'quit' to quit interactive mode.\n\
Type 'help' to get help.\n");
		const char quit_word[] = "quit";
		const char help_word[] = "help";
		const int size = 4096;
		char input_str[size];
		while (true) {
			printf (">");
			fgets(input_str, sizeof(char) * size, stdin);
			int len = strlen(input_str);
			if (len>0 && input_str[len-1]=='\n') {
				input_str[len-1] = '\0';
			}
			if (strcmp(quit_word, input_str) == 0) {
				break;
			} else if (strcmp(help_word, input_str) == 0) {
				print_help();
			} else {
				calculate (input_str);			
			}
		}		
	} else if (argc == 2) {
		calculate (argv[1]);
	} else {
		printf ("I expect only one parameter or no parameters. Try to start interactive mode by starting without parameters.\n");
		return 1;
	}
	eraseUserFuncExpressions();
	return 0;
}
